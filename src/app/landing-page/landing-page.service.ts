import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable, Subject } from "rxjs";

@Injectable()
export class LandingPageService {

  private apiKey = "3d8b309701a13f65b660fa2c64cdc517";
  private subject = new Subject<any>();
  private cityNames = ['London', 'Shcherbinka', 'Dubrovitsy', 'Butovo', 'Lesparkkhoz'];

  constructor(private httpClient: HttpClient) { }

  /** GET Relevant data for the cities */
  getCities() {
    let cityList: any = [];
    this.cityNames.forEach((city) => {
      cityList.push(this.httpClient.get('http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=' + this.apiKey));
    });
    forkJoin(cityList).subscribe(results => {
      console.dir(results);
      this.subject.next(results);
    });

  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
